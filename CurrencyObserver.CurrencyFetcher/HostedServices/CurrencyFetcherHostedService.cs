﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CurrencyObserver.CurrencyFetcher.Clients.CurrencyObserver.Interfaces;
using CurrencyObserver.CurrencyFetcher.Clients.Interfaces;
using CurrencyObserver.CurrencyFetcher.Models;
using CurrencyObserver.CurrencyFetcher.Models.Configs;
using CurrencyObserver.CurrencyFetcher.Models.Enums;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CurrencyObserver.CurrencyFetcher.HostedServices
{
    public class CurrencyFetcherHostedService : BackgroundService
    {
        private readonly ICurrencyRatesClientFactory _currencyRatesClientFactory;
        private readonly ICurrencyObserverClient _currencyObserverClient;
        private readonly ILogger<CurrencyFetcherHostedService> _logger;
        private readonly CurrencyRatesPollingConfigWrapper _config;

        private ICurrencyRatesClient _currencyRatesClient;
        private CurrencyClientType _currentClient;

        public CurrencyFetcherHostedService(
            ICurrencyRatesClientFactory currencyRatesClientFactory,
            CurrencyRatesPollingConfigWrapper config,
            ICurrencyObserverClient currencyObserverClient,
            ILogger<CurrencyFetcherHostedService> logger)
        {
            _currencyRatesClientFactory = currencyRatesClientFactory;
            _currencyObserverClient = currencyObserverClient;
            _logger = logger;
            _config = config;
            _currentClient = _config.Client;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("App is started");

            _currencyRatesClient = _currencyRatesClientFactory.Create(_config.Client);

            while (!stoppingToken.IsCancellationRequested)
            {
                if (_config.Client != _currentClient)
                {
                    _currentClient = _config.Client;
                    _currencyRatesClient = _currencyRatesClientFactory.Create(_currentClient);
                }

                _logger.LogInformation("Fetching currency rates");

                var currencies = _config.Currencies;
                var exchangeRates = await _currencyRatesClient.GetExchangeRates(currencies, stoppingToken);
                var lastExchangeRates = ClearRepeatedExchanges(exchangeRates).ToArray();

                try
                {
                    await _currencyObserverClient.SendExchangeRates(lastExchangeRates, stoppingToken);
                }
                catch (HttpRequestException e)
                {
                    _logger.LogWarning(e, "CurrencyObserver api is unavailable");
                }

                await Task.Delay(_config.FetchingPeriod, stoppingToken);
            }
        }

        private static IEnumerable<ExchangeRateSummary> ClearRepeatedExchanges(
            IEnumerable<ExchangeRateSummary> exchangeRates)
        {
            exchangeRates = exchangeRates.ToArray();

            return exchangeRates
                .OrderByDescending(rate => rate.LastUpdatedAtUtc)
                .GroupBy(rate => new {rate.Source, rate.Destination})
                .Select(rate => rate.First());
        }
    }
}
