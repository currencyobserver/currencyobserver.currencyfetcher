﻿using System;
using System.Threading.Tasks;
using CurrencyObserver.CurrencyFetcher.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CurrencyObserver.CurrencyFetcher
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await new HostBuilder()
                .ConfigureAppConfiguration(BuildAppConfig)
                .ConfigureLogging(builder =>
                {
                    builder.ClearProviders();
                    builder.AddConsole();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddEssentials(hostContext.Configuration);
                })
                .RunConsoleAsync();
        }

        private static void BuildAppConfig(HostBuilderContext context, IConfigurationBuilder builder)
        {
            var env = context.HostingEnvironment;
            context.Configuration = builder.SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
