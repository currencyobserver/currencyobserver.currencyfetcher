﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using CurrencyObserver.CurrencyFetcher.Clients.CurrencyObserver.Interfaces;
using CurrencyObserver.CurrencyFetcher.Clients.CurrencyObserver.Requests;
using CurrencyObserver.CurrencyFetcher.Models;

namespace CurrencyObserver.CurrencyFetcher.Clients.CurrencyObserver
{
    public class CurrencyObserverClient : ICurrencyObserverClient
    {
        private readonly HttpClient _httpClient;

        public CurrencyObserverClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task SendExchangeRates(
            IEnumerable<ExchangeRateSummary> exchangeRates,
            CancellationToken cancellationToken)
        {
            var request = new CreateExchangeRatesRequest
            {
                ExchangeRates = exchangeRates
            };

            var content = CreateJsonContent(request);
            var response = await _httpClient.PostAsync("exchange-rate/batch", content, cancellationToken);

            response.EnsureSuccessStatusCode();
        }

        private static StringContent CreateJsonContent(object request)
        {
            var content = JsonSerializer.Serialize(request);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

            return stringContent;
        }
    }
}
