﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CurrencyObserver.CurrencyFetcher.Models;

namespace CurrencyObserver.CurrencyFetcher.Clients.CurrencyObserver.Interfaces
{
    public interface ICurrencyObserverClient
    {
        Task SendExchangeRates(IEnumerable<ExchangeRateSummary> exchangeRates, CancellationToken cancellationToken);
    }
}
