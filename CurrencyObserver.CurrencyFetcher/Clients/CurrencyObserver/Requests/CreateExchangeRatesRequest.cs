﻿using System.Collections.Generic;
using CurrencyObserver.CurrencyFetcher.Models;

namespace CurrencyObserver.CurrencyFetcher.Clients.CurrencyObserver.Requests
{
    public class CreateExchangeRatesRequest
    {
        public IEnumerable<ExchangeRateSummary> ExchangeRates { get; set; }
    }
}
