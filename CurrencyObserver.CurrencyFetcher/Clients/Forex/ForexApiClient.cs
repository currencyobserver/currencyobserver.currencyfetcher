﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CurrencyObserver.CurrencyFetcher.Clients.Forex.Extensions;
using CurrencyObserver.CurrencyFetcher.Clients.Forex.Responses;
using CurrencyObserver.CurrencyFetcher.Clients.Interfaces;
using CurrencyObserver.CurrencyFetcher.Extensions;
using CurrencyObserver.CurrencyFetcher.Models;
using CurrencyObserver.CurrencyFetcher.Models.Configs;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;

namespace CurrencyObserver.CurrencyFetcher.Clients.Forex
{
    public class ForexApiClient : ICurrencyRatesClient
    {
        private const string CurrenciesSeparator = "/";

        private readonly HttpClient _httpClient;
        private readonly IOptionsMonitor<CurrencyRatesApiConfig> _optionsMonitor;

        public ForexApiClient(HttpClient httpClient, IOptionsMonitor<CurrencyRatesApiConfig> optionsMonitor)
        {
            _httpClient = httpClient;
            _optionsMonitor = optionsMonitor;
        }

        public async Task<IEnumerable<ExchangeRateSummary>> GetExchangeRates(
            IEnumerable<CurrencyExchange> exchanges,
            CancellationToken cancellation)
        {
            var currencies = PrepareCurrencyCodes(exchanges).ToArray();

            var queryParams = new Dictionary<string, string>
            {
                { "symbol", string.Join(",", currencies)}
            };

            var uri = PrepareUri("latest", queryParams);
            var response = await _httpClient.GetAsync(uri, cancellation);
            response.EnsureSuccessStatusCode();

            var responseWrapper = await response.Content.ReadAsAsync<ForexApiResponseWrapper>();

            return responseWrapper.Response.Select(rate => rate.ToExchangeRatesSummary());
        }

        private static IEnumerable<string> PrepareCurrencyCodes(IEnumerable<CurrencyExchange> exchanges)
        {
            return exchanges.Select(exchange => string.Concat(exchange.From, CurrenciesSeparator, exchange.To));
        }

        private Uri PrepareUri(string relativePath, IDictionary<string, string> query)
        {
            var baseEndpoint = _optionsMonitor.CurrentValue.Endpoint;
            var apiKey = _optionsMonitor.CurrentValue.ApiKey;

            var url = $"{baseEndpoint}/{relativePath}";

            query.Add("access_key", apiKey);
            var newUrl = new Uri(QueryHelpers.AddQueryString(url, query));

            return newUrl;
        }

        public void Dispose()
        {
            _httpClient.CancelPendingRequests();
            _httpClient?.Dispose();
        }
    }
}
