﻿using Newtonsoft.Json;

namespace CurrencyObserver.CurrencyFetcher.Clients.Forex.Responses
{
    public class ForexApiResponseWrapper
    {
        public bool Status { get; set; }

        [JsonProperty("msg")]
        public string Message { get; set; }

        public int Code { get; set; }

        public ForesApiExchangeRatesSummaryResponse[] Response { get; set; }
    }
}
