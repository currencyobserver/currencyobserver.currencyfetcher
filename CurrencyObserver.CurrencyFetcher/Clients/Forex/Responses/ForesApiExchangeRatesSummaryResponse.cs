﻿using System;
using Newtonsoft.Json;

namespace CurrencyObserver.CurrencyFetcher.Clients.Forex.Responses
{
    public class ForesApiExchangeRatesSummaryResponse
    {
        [JsonProperty("s")]
        public string FromTo { get; set; }

        [JsonProperty("h")]
        public decimal HighestPrice { get; set; }

        [JsonProperty("l")]
        public decimal LowestPrice { get; set; }

        [JsonProperty("c")]
        public decimal LatestPrice { get; set; }

        [JsonProperty("cp")]
        public string DailyChangeInPercentage { get; set; }

        [JsonProperty("tm")]
        public DateTime LastUpdatedAtUtc { get; set; }
    }
}
