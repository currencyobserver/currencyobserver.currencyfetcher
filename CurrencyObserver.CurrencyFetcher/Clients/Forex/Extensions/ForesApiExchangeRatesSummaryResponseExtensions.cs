﻿using System;
using CurrencyObserver.CurrencyFetcher.Clients.Forex.Responses;
using CurrencyObserver.CurrencyFetcher.Models;

namespace CurrencyObserver.CurrencyFetcher.Clients.Forex.Extensions
{
    public static class ForesApiExchangeRatesSummaryResponseExtensions
    {
        public static ExchangeRateSummary ToExchangeRatesSummary(this ForesApiExchangeRatesSummaryResponse response)
        {
            var fromToValues = response.FromTo.Split("/", StringSplitOptions.RemoveEmptyEntries);

            if (fromToValues.Length != 2)
                throw new InvalidOperationException($"{nameof(response.FromTo)} is in wrong format");

            return new ExchangeRateSummary
            {
                Source = fromToValues[0],
                Destination = fromToValues[1],
                LatestPrice = response.LatestPrice,
                HighestPrice = response.HighestPrice,
                LowestPrice = response.LowestPrice,
                DailyChangeInPercentage = response.DailyChangeInPercentage,
                LastUpdatedAtUtc = response.LastUpdatedAtUtc
            };
        }
    }
}
