﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CurrencyObserver.CurrencyFetcher.Models;

namespace CurrencyObserver.CurrencyFetcher.Clients.Interfaces
{
    public interface ICurrencyRatesClient : IDisposable
    {
        Task<IEnumerable<ExchangeRateSummary>> GetExchangeRates(
            IEnumerable<CurrencyExchange> exchanges,
            CancellationToken cancellation);
    }
}
