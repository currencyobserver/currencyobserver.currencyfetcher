﻿using CurrencyObserver.CurrencyFetcher.Models.Enums;

namespace CurrencyObserver.CurrencyFetcher.Clients.Interfaces
{
    public interface ICurrencyRatesClientFactory
    {
        ICurrencyRatesClient Create(CurrencyClientType type);
    }
}
