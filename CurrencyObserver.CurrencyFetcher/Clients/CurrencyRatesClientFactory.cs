﻿using System;
using CurrencyObserver.CurrencyFetcher.Clients.Forex;
using CurrencyObserver.CurrencyFetcher.Clients.Interfaces;
using CurrencyObserver.CurrencyFetcher.Models.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace CurrencyObserver.CurrencyFetcher.Clients
{
    public class CurrencyRatesClientFactory : ICurrencyRatesClientFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public CurrencyRatesClientFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public ICurrencyRatesClient Create(CurrencyClientType type)
        {
            var client = type switch
            {
                CurrencyClientType.Forex => _serviceProvider.GetRequiredService<ForexApiClient>(),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, "Client is not registered")
            };

            return client;
        }
    }
}
