﻿using System;
using CurrencyObserver.CurrencyFetcher.Clients;
using CurrencyObserver.CurrencyFetcher.Clients.CurrencyObserver;
using CurrencyObserver.CurrencyFetcher.Clients.CurrencyObserver.Interfaces;
using CurrencyObserver.CurrencyFetcher.Clients.Forex;
using CurrencyObserver.CurrencyFetcher.Clients.Interfaces;
using CurrencyObserver.CurrencyFetcher.HostedServices;
using CurrencyObserver.CurrencyFetcher.Models.Configs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace CurrencyObserver.CurrencyFetcher.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddEssentials(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<CurrencyRatesApiConfig>(config.GetSection(nameof(CurrencyRatesApiConfig)));
            services.Configure<CurrencyRatesPollingConfig>(config.GetSection(nameof(CurrencyRatesPollingConfig)));

            services.AddSingleton<ICurrencyRatesClientFactory, CurrencyRatesClientFactory>();
            services.AddSingleton<ForexApiClient>();
            services.AddSingleton<ICurrencyObserverClient, CurrencyObserverClient>();

            services.AddSingleton(sp => new CurrencyRatesPollingConfigWrapper(
                sp.GetRequiredService<IOptionsMonitor<CurrencyRatesPollingConfig>>()));

            services.AddHostedService<CurrencyFetcherHostedService>();

            services.AddHttpClient<ICurrencyObserverClient, CurrencyObserverClient>(client =>
            {
                client.BaseAddress = new Uri(config["CurrencyObserverApi"]);
            });
        }
    }
}
