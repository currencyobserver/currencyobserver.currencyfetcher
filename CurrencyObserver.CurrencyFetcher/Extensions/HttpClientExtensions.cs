﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CurrencyObserver.CurrencyFetcher.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task<T> ReadAsAsync<T>(this HttpContent content)
        {
            var contentValue = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(contentValue);
        }
    }
}
