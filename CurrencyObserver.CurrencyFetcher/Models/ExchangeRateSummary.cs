﻿using System;

namespace CurrencyObserver.CurrencyFetcher.Models
{
    public class ExchangeRateSummary
    {
        public string Source { get; set; }

        public string Destination { get; set; }

        public decimal HighestPrice { get; set; }

        public decimal LowestPrice { get; set; }

        public decimal LatestPrice { get; set; }

        public string DailyChangeInPercentage { get; set; }

        public DateTime LastUpdatedAtUtc { get; set; }
    }
}
