﻿namespace CurrencyObserver.CurrencyFetcher.Models
{
    public class CurrencyExchange
    {
        public string From { get; set; }

        public string To { get; set; }
    }
}
