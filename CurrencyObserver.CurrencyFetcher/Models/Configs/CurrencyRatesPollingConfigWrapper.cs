﻿using System;
using System.Collections.Generic;
using CurrencyObserver.CurrencyFetcher.Models.Enums;
using Microsoft.Extensions.Options;

namespace CurrencyObserver.CurrencyFetcher.Models.Configs
{
    public class CurrencyRatesPollingConfigWrapper
    {
        private CurrencyRatesPollingConfig _config;

        public CurrencyRatesPollingConfigWrapper(IOptionsMonitor<CurrencyRatesPollingConfig> monitor)
        {
            _config = monitor.CurrentValue;

            monitor.OnChange(Listener);
        }

        private void Listener(CurrencyRatesPollingConfig config)
        {
            _config = config;
        }

        public CurrencyClientType Client => _config.Client;

        public TimeSpan FetchingPeriod => _config.FetchingPeriod;

        public IList<CurrencyExchange> Currencies => _config.Currencies;
    }
}
