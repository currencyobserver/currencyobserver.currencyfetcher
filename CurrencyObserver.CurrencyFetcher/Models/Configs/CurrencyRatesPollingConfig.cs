﻿using System;
using System.Collections.Generic;
using CurrencyObserver.CurrencyFetcher.Models.Enums;

namespace CurrencyObserver.CurrencyFetcher.Models.Configs
{
    public class CurrencyRatesPollingConfig
    {
        public TimeSpan FetchingPeriod { get; set; }

        public IList<CurrencyExchange> Currencies { get; set; }

        public CurrencyClientType Client { get; set; }
    }
}
