﻿namespace CurrencyObserver.CurrencyFetcher.Models.Configs
{
    public class CurrencyRatesApiConfig
    {
        public string Endpoint { get; set; }

        public string ApiKey { get; set; }
    }
}
